/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.views;

import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author peedeeboy
 */
public class TdaRowHeaderTable extends JTable
        implements ChangeListener, PropertyChangeListener, TableModelListener {

    private JTable main;
    private Font font;
    
    public TdaRowHeaderTable(JTable table, AbstractTableModel model) {
        super(model);
        main = table;
        font = main.getTableHeader().getFont();
        
        main.addPropertyChangeListener(this);
        main.getModel().addTableModelListener(this);
        
    }

    public void configure() {
        
        //setSelectionModel(main.getSelectionModel());
        
        // Try to stop models getting out of synch when rows added / deleted
        this.setRowSelectionAllowed(false);
        
        
        setAutoCreateColumnsFromModel(false);
        setPreferredScrollableViewportSize(getPreferredSize());
        getColumnModel().getColumn(convertColumnIndexToView(0))
                        .setCellRenderer(new RowNumberRenderer());
        setBorder(main.getTableHeader().getBorder());
        setShowHorizontalLines(false);
        setShowVerticalLines(false);
        getTableHeader().setReorderingAllowed(false);
        setAutoCreateRowSorter(false);
        setDragEnabled(false);
        setRowSorter(null);
        setFocusable(false);
                
    }
    
    

    @Override
    public void stateChanged(ChangeEvent e) {
        JViewport viewport = (JViewport) e.getSource();
        JScrollPane scrollPane = (JScrollPane) viewport.getParent();
        scrollPane.getVerticalScrollBar().setValue(viewport.getViewPosition().y);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectionModel".equals(evt.getPropertyName())) {
            setSelectionModel(main.getSelectionModel());
        }

        if ("rowHeight".equals(evt.getPropertyName())) {
            repaint();
        }

        if ("model".equals(evt.getPropertyName())) {
            main.getModel().addTableModelListener(this);
            revalidate();
        }
    }

    @Override
    public int getRowHeight(int row) {
        int prefferedRowHeight = main.getRowHeight(row);

        if (prefferedRowHeight != super.getRowHeight(row)) {
            super.setRowHeight(row, prefferedRowHeight);
        }

        return prefferedRowHeight;
    }

    
    /* Inner Classes */
    private final class RowNumberRenderer extends DefaultTableCellRenderer {

        private final boolean opaque;

        public RowNumberRenderer() {
            setHorizontalAlignment(JLabel.CENTER);
            final String lafName = UIManager.getLookAndFeel().getName();
            opaque = lafName.equals("Metal") || lafName.equals("Windows Classic");
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (table != null) {
                JTableHeader header = table.getTableHeader();
                if (header != null) {
                    TableCellRenderer defaultRenderer = header.getDefaultRenderer();
                    if (defaultRenderer != null) {
                        Component component = defaultRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        if (component instanceof JComponent) {
                            JComponent jComponent = (JComponent) component;
                            jComponent.setOpaque(opaque);
                            jComponent.setFont(isSelected ? font.deriveFont(Font.BOLD) : font);
                        }
                        return component;
                    } else {
                        setForeground(header.getForeground());
                        setBackground(header.getBackground());
                        setFont(header.getFont());
                    }
                }
            }

            if (isSelected) {
                setFont(getFont().deriveFont(Font.BOLD));
            }

            setText((value == null) ? "" : value.toString());
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));

            return this;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.UndoRedo;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.peedeeboy.oens.tda.actions.DeleteColumnAction;
import org.peedeeboy.oens.tda.actions.DeleteRowAction;
import org.peedeeboy.oens.tda.actions.EditColumnHeaderAction;
import org.peedeeboy.oens.tda.actions.EditRowHeaderAction;
import org.peedeeboy.oens.tda.actions.InsertColumnAction;
import org.peedeeboy.oens.tda.actions.InsertRowAction;
import org.peedeeboy.oens.tda.actions.MoveColumnLeftAction;
import org.peedeeboy.oens.tda.actions.MoveColumnRightAction;
import org.peedeeboy.oens.tda.actions.MoveRowUpAction;
import org.peedeeboy.oens.tda.actions.ResizeColumnsAction;
import org.peedeeboy.oens.tda.dataobject.TdaDataObject;
import org.peedeeboy.oens.tda.actions.MoveRowDownAction;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;
import org.peedeeboy.oens.tda.models.TdaTableModelFactory;

/**
 *
 * @author peedeeboy
 */
@MultiViewElement.Registration(
        displayName = "#LBL_TdaV2b_VISUAL",
        iconBase = "org/peedeeboy/oens/tda/icons/document-table.png",
        mimeType = "application/x-odysseyengine-2da-v2b",
        persistenceType = TopComponent.PERSISTENCE_NEVER,
        preferredID = "TdaVisual",
        position = 2000
)
@NbBundle.Messages("LBL_TdaV2b_VISUAL=Visual")
public final class TdaVisualElement extends JPanel implements MultiViewElement {

    private TdaDataObject obj;
    //private TdabTableModel model;
    private JToolBar toolbar = new JToolBar();
    private static final Logger LOGGER = Logger.getLogger(
            TdaVisualElement.class.getName()); 
    private transient MultiViewElementCallback callback;
    private UndoRedo.Manager manager = new UndoRedo.Manager();
    
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private JScrollPane scrollPane;
    private JTable dataTable;
    private TdaRowHeaderTable rowHeaderTable;
    
    
    private InsertRowAction insertRowAction;
    private DeleteRowAction deleteRowAction;
    private InsertColumnAction insertColumnAction;
    private DeleteColumnAction deleteColumnAction;
    private EditRowHeaderAction editRowHeaderAction;
    private EditColumnHeaderAction editColumnHeaderAction;
    private MoveRowUpAction moveRowUpAction;
    private MoveRowDownAction moveRowDownAction;
    private MoveColumnLeftAction moveColumnLeftAction;
    private MoveColumnRightAction moveColumnRightAction;
    private ResizeColumnsAction resizeColumnsAction;
    
    
    public TdaVisualElement(Lookup lkp) {
        obj = lkp.lookup(TdaDataObject.class);
        assert obj != null;
        
        try {
            setLayout(new BorderLayout());
            loadData();
            initTables();
            initActions();
            initToolbar();
            //initComponents();

        } 
        catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Error loading 2da file {0}",
                    obj.getName());
            Exceptions.printStackTrace(ex);
        }
        
    }
    
    private void loadData() throws IOException {
    
        TdaTableModelFactory tmf = new TdaTableModelFactory();
        Map tableModels = tmf.create2da(obj.getPrimaryFile().asBytes());
        
        dataTableModel = (TdaDataTableModel) tableModels.get("DATA");
        rowHeaderTableModel = (TdaRowHeaderTableModel) 
                tableModels.get("ROWHEADERS");
        
        dataTableModel.addUndoableEditListener(manager);
        
    }
    
    private void initActions() {
        
        resizeColumnsAction = new ResizeColumnsAction(dataTable, 
                rowHeaderTable);
        
        insertRowAction = new InsertRowAction(dataTable,  rowHeaderTable,
             dataTableModel,  rowHeaderTableModel);
        insertRowAction.addUndoableEditListener(manager);
        
        deleteRowAction = new DeleteRowAction(dataTable,  rowHeaderTable,
             dataTableModel,  rowHeaderTableModel);
        deleteRowAction.addUndoableEditListener(manager);
        
        insertColumnAction = new InsertColumnAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel,
             resizeColumnsAction);
        insertColumnAction.addUndoableEditListener(manager);
        
        deleteColumnAction = new DeleteColumnAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel,
             resizeColumnsAction);
        deleteColumnAction.addUndoableEditListener(manager);
        
        editRowHeaderAction = new EditRowHeaderAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel);
        editRowHeaderAction.addUndoableEditListener(manager);
        
        editColumnHeaderAction = new EditColumnHeaderAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel,
             resizeColumnsAction);
        editColumnHeaderAction.addUndoableEditListener(manager);
        
        moveRowUpAction = new MoveRowUpAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel);
        moveRowUpAction.addUndoableEditListener(manager);
        
        moveRowDownAction = new MoveRowDownAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel);
        moveRowDownAction.addUndoableEditListener(manager);
        
        moveColumnLeftAction = new MoveColumnLeftAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel,
             resizeColumnsAction);
        moveColumnLeftAction.addUndoableEditListener(manager);
        
        moveColumnRightAction = new MoveColumnRightAction(dataTable,  
             rowHeaderTable,  dataTableModel,  rowHeaderTableModel,
             resizeColumnsAction);
        moveColumnRightAction.addUndoableEditListener(manager);
        
    }
    
    private void initComponents() {
        initToolbar();
        initTables();
    }
   
    private void initTables() {
    
        // Create the data table
        dataTable = new TdaDataTable(dataTableModel);
                
        //Create the scroll panes and add the table to it.
        scrollPane = new JScrollPane(dataTable);
        
        // Craete row header table
        rowHeaderTable = new TdaRowHeaderTable(dataTable, rowHeaderTableModel);
        rowHeaderTable.configure();
        
        // Add row headers to scrollpane
        scrollPane.setRowHeaderView(rowHeaderTable);
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER,
            rowHeaderTable.getTableHeader());
    
        //Add the scroll pane to this panel.
        add(scrollPane, BorderLayout.CENTER);
        
    }
    
    private void initToolbar() {
        
        // Set toolbar properties
        toolbar.setFloatable (false);
        
        // Seperator
        toolbar.addSeparator();
        
        // Add Row Button
        JButton insertRowButton = new JButton(insertRowAction);
        insertRowButton.setToolTipText("Insert Row");
        insertRowButton.setText("");
        toolbar.add(insertRowButton);
        
        // Delete Row Button
        JButton deleteRowButton = new JButton(deleteRowAction);
        deleteRowButton.setToolTipText("Delete Row");
        deleteRowButton.setText("");
        toolbar.add(deleteRowButton);
        
        // Add Column Button
        JButton insertColumnButton = new JButton(insertColumnAction);
        insertColumnButton.setToolTipText("Insert Column");
        insertColumnButton.setText("");
        toolbar.add(insertColumnButton);
        
        // Delete Column Button
        JButton deleteColumnButton = new JButton(deleteColumnAction);
        deleteColumnButton.setToolTipText("Delete Column");
        deleteColumnButton.setText("");
        toolbar.add(deleteColumnButton);
        
        // Edit Row Header Button
        JButton editRowHeaderButton = new JButton(editRowHeaderAction);
        editRowHeaderButton.setToolTipText("Edit Row Header");
        editRowHeaderButton.setText("");
        toolbar.add(editRowHeaderButton);
        
        // Edit Column Header Button
        JButton editColumnHeaderButton = new JButton(editColumnHeaderAction);
        editColumnHeaderButton.setToolTipText("Edit Column Header");
        editColumnHeaderButton.setText("");
        toolbar.add(editColumnHeaderButton);
        
        // Seperator
        toolbar.addSeparator();
        
        // Move Row Up Button
        JButton moveRowUpButton = new JButton(moveRowUpAction);
        moveRowUpButton.setToolTipText("Move Row Up");
        moveRowUpButton.setText("");
        toolbar.add(moveRowUpButton);
        
        // Move Row Down Button
        JButton moveRowDownButton = new JButton(moveRowDownAction);
        moveRowDownButton.setToolTipText("Move Row Down");
        moveRowDownButton.setText("");
        toolbar.add(moveRowDownButton);
        
        // Move Column Up Button
        JButton moveColumnLeftButton = new JButton(moveColumnLeftAction);
        moveColumnLeftButton.setToolTipText("Move Column Left");
        moveColumnLeftButton.setText("");
        toolbar.add(moveColumnLeftButton);
        
        // Move Column Right Button
        JButton moveColumnRightButton = new JButton(moveColumnRightAction);
        moveColumnRightButton.setToolTipText("Move Column Right");
        moveColumnRightButton.setText("");
        toolbar.add(moveColumnRightButton);
        
        // Seperator
        toolbar.addSeparator();
        
        // Resize Columns Button
        JButton resizeColumnsButton = new JButton(resizeColumnsAction);
        resizeColumnsButton.setToolTipText("Automatically resize columns");
        resizeColumnsButton.setText("");
        toolbar.add(resizeColumnsButton);
        
        // Seperator
        toolbar.addSeparator();
        
        // Save Type
        JLabel saveTypeLabel= new JLabel("Save Type: ");
        JTextField saveTypeTextField = new JTextField(20);
        saveTypeTextField.setText("Binary");
        saveTypeTextField.setSize(
            new Dimension(100, saveTypeTextField.getPreferredSize().height));
        saveTypeTextField.setMaximumSize(
            new Dimension(100, saveTypeTextField.getPreferredSize().height));
        saveTypeTextField.setMinimumSize(
            new Dimension(100, saveTypeTextField.getPreferredSize().height));
        saveTypeTextField.setEditable(false);
        saveTypeTextField.setFocusable(false);
        saveTypeLabel.setLabelFor(saveTypeTextField);
        toolbar.add(saveTypeLabel);
        toolbar.add(saveTypeTextField);
    }
    
    @Override
    public String getName() {
        return "TdaV2bVisualElement";
    }
    
    @Override
    public JComponent getVisualRepresentation() {
        return this;
    }

    @Override
    public JComponent getToolbarRepresentation() {
        return toolbar;
    }

    @Override
    public Action[] getActions() {
        return new Action[0];
    }

    @Override
    public Lookup getLookup() {
        return obj.getLookup();
    }

    @Override
    public void componentOpened() {
        callback.getTopComponent().setDisplayName(
                obj.getPrimaryFile().getNameExt());
    }

    @Override
    public void componentClosed() {
        
    }

    @Override
    public void componentShowing() {
        resizeColumnsAction.actionPerformed(
                new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
    }

    @Override
    public void componentHidden() {
        
    }

    @Override
    public void componentActivated() {
        callback.getTopComponent().setDisplayName(
                obj.getPrimaryFile().getNameExt());
    }

    @Override
    public void componentDeactivated() {
        
    }

    @Override
    public UndoRedo getUndoRedo() {
        return manager;
    }

    @Override
    public void setMultiViewCallback(MultiViewElementCallback callback) {
        this.callback = callback;
    }

    @Override
    public CloseOperationState canCloseElement() {
        return CloseOperationState.STATE_OK;
    }
    
    
    /* Inner Classes */
    
}

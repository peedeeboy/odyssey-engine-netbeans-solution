/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.views;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author peedeeboy
 */
public class TdaDataTable extends JTable{
    
    private static final Logger LOGGER = Logger.getLogger(
            TdaDataTable.class.getName());
    
    public TdaDataTable(AbstractTableModel model) {
        super(model);
        configure();
    }
    
    public void configure() {
    
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        getTableHeader().setReorderingAllowed(false);
        setFillsViewportHeight(false); 
        setShowGrid(false);
        setAutoCreateRowSorter(false);
        setDragEnabled(false);
        setDropMode(javax.swing.DropMode.ON_OR_INSERT_ROWS);
        setRowHeight(25);
        setRowSorter(null);
        
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        setColumnSelectionAllowed(true);
        this.setRowSelectionAllowed(true);
        
        LOGGER.log(Level.INFO, "Data table column count: {0}",
                this.getColumnModel().getColumnCount());
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@TemplateRegistration(
        folder = "Odyssey Engine",
        content = "dataobject/TdaTemplate.2da",
        displayName = "2DA v2 (Binary)",
        requireProject = true,
        description = "dataobject/TdaDescription.html")
package org.peedeeboy.oens.tda;

import org.netbeans.api.templates.TemplateRegistration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.models;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

/**
 *
 * @author peedeeboy
 */
public class TdaTableModelFactory {

    /* Properties */
    private InputStream bs;
    private int r;                                                              // Variable to byte read in decimal
    private char ch;                                                            // Variable used to covert byte to ASCII
    private int filesize;                                                       // Variable to hold file size
    private String header;                                                      // Variable to store file header
    private int rowCount = 0;                                                   // Variable to store the row count
    private int columnCount = 0;                                                // Variable to store the column count
    private int cellCount = 0;                                                  // Variable to store the cell count
    private int[] offsets;                                                      // Array to hold offsets
    private List<String> columnHeaders = new ArrayList<>();                     // List to store the column names
    private List<String> rowHeaders = new ArrayList<>();                        // List to store row names
    private List<List<String>> data = new ArrayList<>();

    private static final Logger LOGGER = Logger.getLogger(
            TdaTableModelFactory.class.getName());

    final int NEWLINE = 10;                                                     // Constant for a ASCILI newline char
    final int NULL = 0;                                                         // Constant for an ASCII null
    final int TAB = 9;                                                          // Constant for an ASCII TAB

    /* Methods */
    public TdaTableModelFactory() {
    }

    public Map<String, Object> create2da(byte[] bytes) {

        LOGGER.log(Level.INFO, "Conveting binary 2da to TabelModel");
        filesize = bytes.length;
        bs = new ByteArrayInputStream(bytes);

        try {
            readHeader();
            readColumnHeaders();
            readRowCount();
            readRowHeaders();
            readOffsets();
            skipUnusedBytes();
            readData();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Error parsing 2da file");
            Exceptions.printStackTrace(ex);
        }
        
        Map<String, Object> tableModels = new HashMap<>();
       
        tableModels.put("DATA", new TdaDataTableModel(columnHeaders, data));
        tableModels.put("ROWHEADERS", new TdaRowHeaderTableModel(rowHeaders));

        return tableModels;

    }

    private void readHeader() throws IOException {

        LOGGER.log(Level.INFO, "Reading 2da header");

        r = bs.read();                                                          // Read first byte
        while (r != NEWLINE) {                                                  // Read bytes until we hit a newline char
            ch = (char) r;                                                      // Convert decimal to char
            header += ch;                                                       // Add char to header
            r = bs.read();                                                      // Read next byte
        }
    }

    private void readColumnHeaders() throws IOException {

        LOGGER.log(Level.INFO, "Reading 2da column names");

        r = bs.read();                                                          // Read next byte
        while (r != NULL) {                                                     // Read bytes until we hit a null char

            String column = "";                                                 // Variable to 'gather' column name

            while (r != TAB) {                                                   // Read bytes until we hit a tab
                ch = (char) r;                                                  // Convert decimal to char
                column += ch;                                                   // Add char to column name
                r = bs.read();                                                  // Read next byte
            }
            r = bs.read();                                                      // Read next byte
            columnCount++;                                                      // Increment column count
            columnHeaders.add(column);                                          // Add column to list of columnNames

        }
    }

    private void readRowCount() throws IOException {

        LOGGER.log(Level.INFO, "Reading 2da row count");
        byte[] byteArray = new byte[4];                                         // Create a byte array of four bytes (to hold 32 to bit int)
        for (int i = 0; i < 4; i++) {                                            // Read next four bytes
            byteArray[i] = (byte) bs.read();                                    // into the byte array
        }

        ByteBuffer buffer = ByteBuffer.wrap(byteArray);                         // Stuff the byte array into a new Byte Buffer
        buffer.order(ByteOrder.LITTLE_ENDIAN);                                  // Convert the byte order of the int from BIG_ENDIAN to LITTLE_ENDIAN
        rowCount = buffer.getInt();                                             // Get rowCount
        
    }

    private void readRowHeaders() throws IOException {

        LOGGER.log(Level.INFO, "Reading 2da row headers");
        for (int i = 0; i < rowCount; i++) {                                     // Iterate once for each row

            String row = "";                                                // Variable to 'gather' row name
            r = bs.read();                                                  // Read next byte
            while (r != TAB) {                                               // Read bytes until we hit a TAB char
                ch = (char) r;                                              // Convert decimal to char
                row += ch;                                                  // Add char to row name
                r = bs.read();                                              // Read next byre
            }

            rowHeaders.add(row);                                            // Add row name to list

        }

    }

    private void readOffsets() throws IOException {

        LOGGER.log(Level.INFO, "Reading 2da offsets");
        cellCount = rowCount * columnCount;                                     // Calculate number of offsets
        offsets = new int[cellCount];                                           // Create array to hold offsets

        for (int i = 0; i < cellCount; i++) {                                    // Iterate once for each offset

            byte[] offsetByteArray = new byte[2];                               // Create new two byte array to store 16 bit number

            for (int j = 0; j < 2; j++) {                                        // Get next two bytes 
                offsetByteArray[j] = (byte) bs.read();                          // and put them in the array
            }

            ByteBuffer offBuf = ByteBuffer.wrap(offsetByteArray);               // Stuff the byte array a new byte buffer
            offBuf.order(ByteOrder.LITTLE_ENDIAN);                              // Convert the byte order to LITTE_ENDIAN
            int offset = offBuf.getChar();                                      // Get the 16 bit number (Pop fact: char is a 16 bit unsigned number datatype!)

            offsets[i] = offset;                                                // Put offset in array

        }

    }

    private void skipUnusedBytes() throws IOException {
        LOGGER.log(Level.INFO, "Skipping unused bytes");
        bs.skip(2);                                                         // TODO: check these bytes are both NULL
    }

    private void readData() throws IOException {
        
        LOGGER.log(Level.INFO, "Reading 2da data section");
        bs.mark(filesize);                                                      // Mark start of data area
        
        int sent = 0;

        for (int row = 0; row < rowCount; row++) {                              // Outer loop: Iterate through data rows
            
            List<String> dataRow = new ArrayList<>();                           // Create new row for 2da
            
            for (int col = 0; col < columnCount; col++) {                       // Inner loop: Iterate through columns

                String value = "";                                              // Variable to 'gather' data value
                System.out.print("Current value: " + value);
                System.out.print("Current offset: " + sent);
                System.out.print("Bytes to skip: " + offsets[sent]);
                
                long skip = bs.skip((long) offsets[sent]);                      // Skip pointer to the offset
                r = bs.read();                                                  // Read next byte

                while (r != NULL && r != -1) {                                  // Read until we hit a null char
                    ch = (char) r;                                              // Convert decimal to ASCII char
                    value += ch;                                                // Add char to value string

                    r = bs.read();                                              // Read next byte
                }

                sent++;                                                         // Increment variable
                dataRow.add(value);                                             // Add value to row
                bs.reset();                                                     // Reset mark ready for next datum
            }
            
            data.add(dataRow);                                                  // Add datarow to 2da
            
        }
    }
    

}

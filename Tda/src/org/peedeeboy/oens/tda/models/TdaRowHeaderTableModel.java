/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author peedeeboy
 */
public class TdaRowHeaderTableModel extends AbstractTableModel {
    
    /* Properties */
    private List<String> columnHeaders;
    private List<String> rowHeaders;
    
    /* Methods */
    public TdaRowHeaderTableModel() {
        
        columnHeaders = new ArrayList<>();
        columnHeaders.add("#");
        rowHeaders = new ArrayList<>();
        
    }
    
    public TdaRowHeaderTableModel(List<String> rowHeaders) {
        
        columnHeaders = new ArrayList<>();
        columnHeaders.add("#");
        this.rowHeaders = rowHeaders;
        
    }
    
    public void removeRow(int rowIndex) {
        
        rowHeaders.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);

    }

    public void addRow(int rowIndex, String header) {
        
        if(rowIndex >= rowHeaders.size()) {
            rowHeaders.add(header);
        }
        else {
            rowHeaders.add(rowIndex, header);
        }
        fireTableRowsInserted(rowIndex, rowIndex);
    }
    
    @Override
    public int getRowCount() {
        return rowHeaders.size();
    }

    @Override
    public int getColumnCount() {
        return columnHeaders.size();
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        return rowHeaders.get(rowIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        rowHeaders.set(rowIndex, aValue.toString());
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public String getColumnName(int column) {
        return columnHeaders.get(column);
    }
    
    public void moveRowUp(int rowIndex) {
     
        if(rowIndex > 0) {
            
            int rowFrom = rowIndex;
            int rowTo = rowIndex - 1;
            
            Collections.swap(rowHeaders, rowFrom, rowTo);
            
            this.fireTableRowsUpdated(rowTo, rowFrom);
        }
    }
    
     public void moveRowDown(int rowIndex) {
     
        if(rowIndex < rowHeaders.size() - 1) {
            
            int rowFrom = rowIndex;
            int rowTo = rowIndex + 1;
            
            Collections.swap(rowHeaders, rowFrom, rowTo);
            
            this.fireTableRowsUpdated(rowFrom, rowTo);
        }
    }

    @Override
    public String toString() {
               
        return "TdaRowHeaderTableModel{" + 
                "columnHeaders=" + columnHeaders + 
                ", rowHeaders=" + rowHeaders + '}';
    }
    
    
    
            
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.event.TableModelEvent;
import static javax.swing.event.TableModelEvent.HEADER_ROW;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;

/**
 *
 * @author peedeeboy
 */
public class TdaDataTableModel extends AbstractTableModel {
    
    /* Properties */
    private List<String> columnHeaders;
    private List<List<String>> data;
    private UndoableEditSupport undoSupport;
    
    
    /* Methods */
    public TdaDataTableModel() {
        
        columnHeaders = new ArrayList<>();
        data = new ArrayList<>();
        undoSupport = new UndoableEditSupport();
        
    }
    
    public TdaDataTableModel(List<String> columnHeaders, 
            List<List<String>> data) {
        
        this.columnHeaders = columnHeaders;
        this.data = data;
        undoSupport = new UndoableEditSupport();

    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnHeaders.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnHeaders.get(column);
    }
    
    public void setColumnName(String aValue, int columnIndex) {
        columnHeaders.set(columnIndex, aValue);
        this.fireTableStructureChanged();
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        return data.get(rowIndex).get(columnIndex);
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String newValue = aValue.toString();
        String oldValue = getValueAt(rowIndex, columnIndex);
        
        data.get(rowIndex).set(columnIndex, newValue);
        fireUndoableCellEdit(rowIndex, columnIndex, oldValue, newValue);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    public List<String> getRow(int rowIndex) {
        return data.get(rowIndex);
    }
    
    private void undoRedoValueAt(String aValue, int rowIndex, int columnIndex) {
        data.get(rowIndex).set(columnIndex, aValue);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableCellEdit(int rowIndex, int columnIndex, 
                String oldValue, String newValue) {
         UndoableEdit edit = new UndoableCellEdit(rowIndex, columnIndex,
                 oldValue, newValue);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
     }
    
    public boolean canDeleteRow() {
        
        return data.size() > 1;
        
    }
    
    public boolean canDeleteColumn() {
        
        return columnHeaders.size() > 1;
        
    }
    
    public void removeRow(int rowIndex) {
        data.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    

    public void insertRow(int rowIndex, List<String> rowData) {
      
        if(rowIndex >= data.size()) {
            data.add(rowData);
        }
        else {
            data.add(rowIndex, rowData);
        }
        
        fireTableRowsInserted(rowIndex, rowIndex);
        
    }

    
    public void addRow(int rowIndex) {
        
        List<String> newRow = new ArrayList<>();
        columnHeaders.forEach(x -> newRow.add(""));
        
        if(rowIndex >= data.size()) {
            data.add(newRow);
        }
        else {
            data.add(rowIndex, newRow);
        }
        
        fireTableRowsInserted(rowIndex, rowIndex);
        
    }
   
    public void removeColumn(int columnIndex) {
        
        columnHeaders.remove(columnIndex);
        data.forEach(row -> row.remove(columnIndex));
        
        fireTableStructureChanged();
        
    }
    
    public void insertColumn(int columnIndex, String columnHeader, 
            List<String>columnData) {
        
        if(columnIndex >= columnHeaders.size()) {
            columnHeaders.add(columnHeader);
            for(int i = 0; i < data.size(); i++) {
                data.get(i).add(columnData.get(i));
            }
        }
        else {
            columnHeaders.add(columnIndex, columnHeader);
            for(int i = 0; i < data.size(); i++) {
                data.get(i).add(columnIndex, columnData.get(i));
            }
        }
        
        fireTableStructureChanged();
    }
    
    public void addColumn(int columnIndex, String columnHeader) {
        
        if(columnIndex >= columnHeaders.size()) {
            columnHeaders.add(columnHeader);
            data.forEach(row -> row.add(""));
        }
        else {
            columnHeaders.add(columnIndex, columnHeader);
            data.forEach(row -> row.add(columnIndex, ""));
        }
        
        fireTableStructureChanged();
    }
    
    public void moveRowUp(int rowIndex) {
     
        if(rowIndex > 0) {
            
            int rowFrom = rowIndex;
            int rowTo = rowIndex - 1;

            Collections.swap(data, rowFrom, rowTo);
            
            fireTableRowsUpdated(rowTo, rowFrom);
        }
    }
    
    public void moveRowDown(int rowIndex) {
     
        if(rowIndex < data.size() - 1) {
            
            int rowFrom = rowIndex;
            int rowTo = rowIndex + 1;
            
            Collections.swap(data, rowFrom, rowTo);
            
            fireTableRowsUpdated(rowFrom, rowTo);
        }
    }
    
    public void moveColumnLeft(int columnIndex) {
        
        if(columnIndex > 0) {
            
            int columnFrom = columnIndex;
            int columnTo = columnIndex - 1;
            
            data.forEach(row -> Collections.swap(row, columnFrom, columnTo));
            Collections.swap(columnHeaders, columnFrom, columnTo);
            
            fireTableStructureChanged();
        }
    }
    
    public void moveColumnRight(int columnIndex) {
        
        if(columnIndex < columnHeaders.size() - 1) {
            
            int columnFrom = columnIndex;
            int columnTo = columnIndex + 1;
            
            data.forEach(row -> Collections.swap(row, columnFrom, columnTo));
            Collections.swap(columnHeaders, columnFrom, columnTo);
            
            fireTableStructureChanged();
        }
    }
    
    @Override
    public String toString() {
        
        String colString = String.join(", ", columnHeaders);
        String dataString = String.join("\n", data.toString());
        
        return "TdaDataTableModel{" + 
                "columnHeaders=" + colString + 
                ", data=" + dataString + 
                ", undoSupport=" + undoSupport + '}';
    }
    
    
    
    
    /* Inner Classes */
     public class UndoableCellEdit extends AbstractUndoableEdit {
        
        /* Properties */
        private int rowIndex;
        private int columnIndex;
        private String oldValue;
        private String newValue;
        
        /* Methods */
        public UndoableCellEdit(int rowIndex, int columnIndex, 
                String oldValue, String newValue) {
            
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            this.oldValue = oldValue;
            this.newValue = newValue;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            undoRedoValueAt(oldValue, rowIndex, columnIndex);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            undoRedoValueAt(newValue, rowIndex, columnIndex);
        }
        
    }
     
}

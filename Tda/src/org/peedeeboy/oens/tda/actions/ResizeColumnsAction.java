/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import org.peedeeboy.oens.tda.views.TdaDataTable;
import org.peedeeboy.oens.tda.views.TdaRowHeaderTable;

/**
 *
 * @author peedeeboy
 */
public class ResizeColumnsAction extends AbstractAction {
    
    private JTable dataTable;
    private JTable rowHeaderTable;
    
    
    public ResizeColumnsAction(JTable dataTable, JTable rowHeaderTable) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-resize-columns.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "DeleteColumnAction");
        
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        updateColumnWidths(rowHeaderTable);
        updateColumnWidths(dataTable);
        
    }
    
    
    public void updateColumnWidths(JTable table) {
        for (int column = 0; column < table.getColumnCount(); column++) {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows
                if (preferredWidth >= maxWidth) {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth(preferredWidth + 20);
        }
    }
    
}

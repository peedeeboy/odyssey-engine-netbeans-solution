/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

public class DeleteRowAction extends AbstractAction {

    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    
    
    public DeleteRowAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel, 
            TdaRowHeaderTableModel rowHeaderTableModel) {  
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-delete-row.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "DeleteRowAction");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        int dataSelectedRow = dataTable.getSelectedRow();
        int headerSelectedRow = rowHeaderTable.getSelectedRow();
        
        if(dataSelectedRow > -1) {
            
            int dataModelRow = dataTable.convertRowIndexToModel(
                    dataSelectedRow);
            int headerModelRow = rowHeaderTable.convertRowIndexToModel(
                    dataModelRow);
            
            System.out.println();
            System.out.println("Data Model Row: " + dataModelRow);
            System.out.println("Header Model Row: " + headerModelRow);
            System.out.println();
            
            if(dataTableModel.canDeleteRow()) {
                
                String rowHeader = rowHeaderTableModel.getValueAt(
                        headerModelRow, 0);
                List<String> oldRow = dataTableModel.getRow(dataModelRow);
                    
                dataTableModel.removeRow(dataModelRow);
                rowHeaderTableModel.removeRow(headerModelRow);
                
                fireUndoableRowDelete(dataModelRow, headerModelRow, 
                        oldRow, rowHeader);
                
            }
            else {
                
                NotifyDescriptor nd = new NotifyDescriptor.Message(
                        "Cannot delete row.  2da must have at least one row.", 
                        NotifyDescriptor.ERROR_MESSAGE);
                DialogDisplayer.getDefault().notify(nd);
                
            }
            
        }
        
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableRowDelete(int dataModelRow, int headerModelRow, 
                List<String> oldRow, String oldRowHeader) {
        
         UndoableEdit edit = new DeleteRowAction.UndoableRowDelete(dataModelRow, 
                 headerModelRow, oldRow, oldRowHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableRowDelete extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldDataModelRow;
        private int oldHeaderModelRow;
        private List<String> oldRow;
        private String oldRowHeader;
        
        
        /* Methods */
        public UndoableRowDelete(int dataModelRow, int headerModelRow, 
                List<String> oldRow, String oldRowHeader) {
            
            this.oldDataModelRow = dataModelRow;
            this.oldHeaderModelRow = headerModelRow;
            this.oldRow = oldRow;
            this.oldRowHeader = oldRowHeader;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.insertRow(oldDataModelRow, oldRow);
            rowHeaderTableModel.addRow(oldHeaderModelRow, oldRowHeader);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.removeRow(oldDataModelRow);
            rowHeaderTableModel.removeRow(oldHeaderModelRow);
        }
        
    }
    
}

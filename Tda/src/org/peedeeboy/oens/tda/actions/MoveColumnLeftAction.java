/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class MoveColumnLeftAction extends AbstractAction {

    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    private ResizeColumnsAction resizeColumnsAction;
    
    public MoveColumnLeftAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel,
            ResizeColumnsAction resizeColumnsAction) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        this.resizeColumnsAction = resizeColumnsAction;
        undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-move-column-left.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "MoveColumnLeftAction");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        int dataSelectedColumn = dataTable.getSelectedColumn();
        int dataSelectedRow = dataTable.getSelectedRow();
        
        if(dataSelectedColumn > -1) {

            int dataModelSelectedColumn = dataTable.convertColumnIndexToModel(
                    dataSelectedColumn);
            
            if(dataModelSelectedColumn > 0) {
                
                dataTableModel.moveColumnLeft(dataModelSelectedColumn);
                dataTable.setColumnSelectionInterval(dataSelectedColumn - 1, 
                        dataSelectedColumn - 1);
                dataTable.setRowSelectionInterval(dataSelectedRow,
                        dataSelectedRow);
                
                fireUndoableMoveColumnLeft(dataSelectedColumn, 
                        dataSelectedColumn - 1, dataSelectedRow);
                resizeColumns();
            }
            
        }
        
    }
    
    private void resizeColumns() {
        resizeColumnsAction.actionPerformed(new ActionEvent(this, 
                ActionEvent.ACTION_PERFORMED, null));
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableMoveColumnLeft(int oldColumnIndex, 
            int newColumnIndex, int rowIndex) {
        
         UndoableEdit edit = new MoveColumnLeftAction.UndoableMoveColumnLeft(
                 oldColumnIndex, newColumnIndex, rowIndex);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableMoveColumnLeft extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldColumnIndex;
        private int newColumnIndex;
        private int rowIndex;
        
        /* Methods */
        public UndoableMoveColumnLeft(int oldColumnIndex, int newColumnIndex,
                int rowIndex) {
            
            this.oldColumnIndex = oldColumnIndex;
            this.newColumnIndex = newColumnIndex;
            this.rowIndex = rowIndex;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.moveColumnRight(newColumnIndex);
            dataTable.setColumnSelectionInterval(oldColumnIndex, oldColumnIndex);
            dataTable.setRowSelectionInterval(rowIndex, rowIndex);
            resizeColumns();
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.moveColumnLeft(oldColumnIndex);
            dataTable.setColumnSelectionInterval(newColumnIndex, newColumnIndex);
            dataTable.setRowSelectionInterval(rowIndex, rowIndex);
            resizeColumns();
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class InsertColumnAction extends AbstractAction {
    
    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    private ResizeColumnsAction resizeColumnsAction;
    
    public InsertColumnAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel,
            ResizeColumnsAction resizeColumnsAction) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        this.resizeColumnsAction = resizeColumnsAction;
        this.undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-insert-column.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "InsertColumnAction");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        String columnHeader = "";
        
        NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine(
                "Column Header", "Insert new column");
        nd.setInputText(columnHeader);
        nd.setOptions(new String[]{"Insert before", "Insert after", "Cancel"});
        
        DialogDisplayer.getDefault().notify(nd);
        String response = (String) nd.getValue();
        
        if(response.equals("Insert before") || response.equals(
                "Insert after")) {
            
            columnHeader = nd.getInputText();
            if(columnHeader.equals("")) {
                columnHeader = "Header";
            }
            
            int dataSelectedColumn = dataTable.getSelectedColumn();
            
            if(dataSelectedColumn > -1) {
                
                int dataModelColumn = dataTable.convertColumnIndexToModel(
                        dataSelectedColumn);
                
                if(response.equals("Insert after")) {
                    dataModelColumn++;
                }
                
                dataTableModel.addColumn(dataModelColumn, columnHeader);
                fireUndoableColumnInsert(dataModelColumn, columnHeader);
                resizeColumns();
            }
            
        }
        
    }
    
    private void resizeColumns() {
        resizeColumnsAction.actionPerformed(new ActionEvent(this, 
                ActionEvent.ACTION_PERFORMED, null));
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableColumnInsert(int dataModelColumn, 
               String columnHeader) {
        
         UndoableEdit edit = new InsertColumnAction.UndoableColumnInsert(
                 dataModelColumn, columnHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableColumnInsert extends AbstractUndoableEdit {
        
        /* Properties */
        private int dataModelColumn;
        private String columnHeader;
        
        
        /* Methods */
        public UndoableColumnInsert(int dataModelColumn, String columnHeader) {
            
            this.dataModelColumn = dataModelColumn;
            this.columnHeader = columnHeader;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.removeColumn(dataModelColumn);
            resizeColumns();
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.addColumn(dataModelColumn, columnHeader);
            resizeColumns();
        }
        
    }
}

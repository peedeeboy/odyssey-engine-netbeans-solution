/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class MoveRowDownAction extends AbstractAction {

    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    
    public MoveRowDownAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-move-row-down.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "MoveRowDownAction");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
         int dataSelectedRow = dataTable.getSelectedRow();
        
        if (dataSelectedRow > -1) {

                int dataModelRow = dataTable.convertRowIndexToModel(
                        dataSelectedRow);
                int headerModelRow = rowHeaderTable.convertRowIndexToModel(
                        dataModelRow);
                
                if(dataModelRow < dataTableModel.getRowCount() - 1) {
                    
                    dataTableModel.moveRowDown(dataModelRow);
                    rowHeaderTableModel.moveRowDown(headerModelRow);
                    dataTable.setRowSelectionInterval(dataModelRow + 1, 
                            dataModelRow + 1);
                    
                    fireUndoableMoveRowDown(dataModelRow, dataModelRow + 1);
                }
                
        }
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableMoveRowDown(int oldRowIndex, int newRowIndex) {
        
         UndoableEdit edit = new MoveRowDownAction.UndoableMoveRowDown(
                 oldRowIndex, newRowIndex);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableMoveRowDown extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldRowIndex;
        private int newRowIndex;
        
        
        /* Methods */
        public UndoableMoveRowDown(int oldRowIndex, int newRowIndex) {
            
            this.oldRowIndex = oldRowIndex;
            this.newRowIndex = newRowIndex;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.moveRowUp(newRowIndex);
            rowHeaderTableModel.moveRowUp(newRowIndex);
            dataTable.setRowSelectionInterval(oldRowIndex, oldRowIndex);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.moveRowDown(oldRowIndex);
            rowHeaderTableModel.moveRowDown(oldRowIndex);
            dataTable.setRowSelectionInterval(newRowIndex, newRowIndex);
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class EditRowHeaderAction extends AbstractAction {

    /* Properties */
    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    
    /* Methods */
    public EditRowHeaderAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-edit-row-header.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "EditRowHeaderAction");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        int dataSelectedRow = dataTable.getSelectedRow();
        int headerSelectedRow = rowHeaderTable.getSelectedRow();                // Get selected row in header table
        
        if(dataSelectedRow > -1) {                                              // Check a row is actually selected
            
            int headerModelRow = rowHeaderTable.convertRowIndexToModel(
                    dataSelectedRow);                                           // Convert Table row to Model row
            String oldRowHeader = rowHeaderTableModel.getValueAt(headerModelRow, 
                    0);                                                         // Get the existing row header
            
            NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine(
                "Row Header", "Edit row header");
            nd.setInputText(oldRowHeader);                                      // Create new dialog
            nd.setOptionType(NotifyDescriptor.OK_CANCEL_OPTION);                // Set dialog to have OK / CANCEL buttons
            
            Object status = DialogDisplayer.getDefault().notify(nd);            // Show dialog and check response
            
            if(status == NotifyDescriptor.OK_OPTION) {                          // User clicked OK, so process
                
                String rowHeader = nd.getInputText();
                if(rowHeader.equals("")) {
                    rowHeader = "Header";
                }
                
                rowHeaderTableModel.setValueAt(rowHeader, headerModelRow, 0);   // Update model with new header
                fireUndoableRowHeaderUpdate(headerModelRow, 
                        oldRowHeader, rowHeader);
                
            }
        }
        
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableRowHeaderUpdate(int headerModelRow, 
            String oldRowHeader, String newRowHeader) {
        
         UndoableEdit edit = new EditRowHeaderAction.UndoableRowHeaderUpdate(
                 headerModelRow, oldRowHeader, newRowHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableRowHeaderUpdate extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldHeaderModelRow;
        private String oldRowHeader;
        private String newRowHeader;
        
        
        /* Methods */
        public UndoableRowHeaderUpdate(int headerModelRow, 
                String oldRowHeader, String newRowHeader) {
            
            this.oldHeaderModelRow = headerModelRow;
            this.oldRowHeader = oldRowHeader;
            this.newRowHeader = newRowHeader;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            rowHeaderTableModel.setValueAt(oldRowHeader, 
                    oldHeaderModelRow, 0);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            rowHeaderTableModel.setValueAt(newRowHeader, 
                    oldHeaderModelRow, 0);
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class DeleteColumnAction extends AbstractAction {

    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    private ResizeColumnsAction resizeColumnsAction;

    public DeleteColumnAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel,
            ResizeColumnsAction resizeColumnsAction) {

        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        this.resizeColumnsAction = resizeColumnsAction;
        this.undoSupport = new UndoableEditSupport();

        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-delete-column.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "DeleteColumnAction");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int dataSelectedColumn = dataTable.getSelectedColumn();

        if (dataSelectedColumn > -1) {

            int dataModelColumn = dataTable.convertColumnIndexToModel(
                    dataSelectedColumn);

            String oldHeader = dataTableModel.getColumnName(dataModelColumn);
            List<String> oldColumn = new ArrayList<>();

            int noRows = dataTableModel.getRowCount();
            for (int i = 0; i < noRows; i++) {
                String value = dataTableModel.getValueAt(i, dataModelColumn);
                oldColumn.add(value);
            }

            dataTableModel.removeColumn(dataModelColumn);
            fireUndoableColumnDelete(dataModelColumn, oldColumn, oldHeader);
            resizeColumns();
            
        }

    }

    private void resizeColumns() {
        resizeColumnsAction.actionPerformed(new ActionEvent(this,
                ActionEvent.ACTION_PERFORMED, null));
    }

    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableColumnDelete(int dataModelColumn, 
                List<String> oldColumn, String oldColumnHeader) {
        
         UndoableEdit edit = new DeleteColumnAction.UndoableColumnDelete(
                 dataModelColumn,oldColumn, oldColumnHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableColumnDelete extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldDataModelColumn;
        private List<String> oldColumn;
        private String oldColumnHeader;
        
        
        /* Methods */
        public UndoableColumnDelete(int oldDataModelColumn,  
                List<String> oldColumn, String oldColumnHeader) {
            
            this.oldDataModelColumn = oldDataModelColumn;
            this.oldColumn = oldColumn;
            this.oldColumnHeader = oldColumnHeader;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.insertColumn(oldDataModelColumn, oldColumnHeader, 
                    oldColumn);
            resizeColumns();
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.removeColumn(oldDataModelColumn);
            resizeColumns();
        }
        
    }
     
}

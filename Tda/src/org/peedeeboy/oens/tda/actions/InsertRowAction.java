/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class InsertRowAction extends AbstractAction {

    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;

    public InsertRowAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel) {

        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        undoSupport = new UndoableEditSupport();

        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-insert-row.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "InsertRowAction");

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String rowHeader = "";
        
        NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine(
                "Row Header", "Insert new row");
        nd.setInputText(rowHeader);
        nd.setOptions(new String[]{"Insert above", "Insert below", "Cancel"});
        
        DialogDisplayer.getDefault().notify(nd);
        String response = (String) nd.getValue();
        
        if (response.equals("Insert above") || response.equals(
                "Insert below")) {

            rowHeader = nd.getInputText();
            if(rowHeader.equals("")) {
                rowHeader = "Header";
            }
            
            int dataSelectedRow = dataTable.getSelectedRow();
            int headerSelectedRow = rowHeaderTable.getSelectedRow();

            if (dataSelectedRow > -1) {

                int dataModelRow = dataTable.convertRowIndexToModel(
                        dataSelectedRow);
                int headerModelRow = rowHeaderTable.convertRowIndexToModel(
                        dataModelRow);

                if(response.equals("Insert below")) {
                    dataModelRow++;
                    headerModelRow++;
                }

                dataTableModel.addRow(dataModelRow);
                rowHeaderTableModel.addRow(headerModelRow, rowHeader);
                
                fireUndoableRowInsert(dataModelRow, headerModelRow, 
                        rowHeader);

            }

        }

    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableRowInsert(int dataModelRow, int headerModelRow, 
               String oldRowHeader) {
        
         UndoableEdit edit = new InsertRowAction.UndoableRowInsert(dataModelRow, 
                 headerModelRow, oldRowHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableRowInsert extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldDataModelRow;
        private int oldHeaderModelRow;
        private List<String> oldRow;
        private String oldRowHeader;
        
        
        /* Methods */
        public UndoableRowInsert(int dataModelRow, int headerModelRow, 
                String oldRowHeader) {
            
            this.oldDataModelRow = dataModelRow;
            this.oldHeaderModelRow = headerModelRow;
            this.oldRowHeader = oldRowHeader;
        
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.removeRow(oldDataModelRow);
            rowHeaderTableModel.removeRow(oldHeaderModelRow);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.addRow(oldDataModelRow);
            rowHeaderTableModel.addRow(oldHeaderModelRow, oldRowHeader);
        }
        
    }

}

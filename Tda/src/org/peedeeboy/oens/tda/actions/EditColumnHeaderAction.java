/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peedeeboy.oens.tda.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.peedeeboy.oens.tda.models.TdaDataTableModel;
import org.peedeeboy.oens.tda.models.TdaRowHeaderTableModel;

/**
 *
 * @author peedeeboy
 */
public class EditColumnHeaderAction extends AbstractAction {

    /* Properties */
    private JTable dataTable;
    private JTable rowHeaderTable;
    private TdaDataTableModel dataTableModel;
    private TdaRowHeaderTableModel rowHeaderTableModel;
    private UndoableEditSupport undoSupport;
    private ResizeColumnsAction resizeColumnsAction;
    
    /* Methods */
    
    public EditColumnHeaderAction(JTable dataTable, JTable rowHeaderTable,
            TdaDataTableModel dataTableModel,
            TdaRowHeaderTableModel rowHeaderTableModel,
            ResizeColumnsAction resizeColumnsAction) {
        
        this.dataTable = dataTable;
        this.rowHeaderTable = rowHeaderTable;
        this.dataTableModel = dataTableModel;
        this.rowHeaderTableModel = rowHeaderTableModel;
        this.resizeColumnsAction = resizeColumnsAction;
        undoSupport = new UndoableEditSupport();
        
        Icon icon = new ImageIcon(getClass().getResource(
                "/org/peedeeboy/oens/tda/icons/table-edit-column-header.png"));
        putValue(SMALL_ICON, icon);
        putValue(NAME, "DeleteColumnAction");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        int dataSelectedColumn = dataTable.getSelectedColumn();                 // Get index of selected column in UI
        
        if(dataSelectedColumn > -1) {                                           // Check a column is actually selected
            
            int dataModelColumn = dataTable.convertColumnIndexToModel(
                    dataSelectedColumn);                                        // Convert UI index to Model index
            String oldColumnHeader = dataTableModel.getColumnName(
                    dataModelColumn);                                           // Get existing column name
        
            NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine(
                "Column Header", "Edit column header");
            nd.setInputText(oldColumnHeader);                                   // Create new dialog
            nd.setOptionType(NotifyDescriptor.OK_CANCEL_OPTION);                // Set dialog to have OK / CANCEL buttons
            
            Object status = DialogDisplayer.getDefault().notify(nd);            // Show dialog and check response
            
            if(status == NotifyDescriptor.OK_OPTION) {                          // User clicked OK, so process
                
                String columnHeader = nd.getInputText();                        // Get new column header from dialog
                if(columnHeader.equals("")) {                                   // If user entered a blank value,
                    columnHeader = "Header";                                    // use default of "Header"
                }
                
                dataTableModel.setColumnName(columnHeader, dataModelColumn);    // Update model with new header
                fireUndoableColumnHeaderUpdate(dataModelColumn, 
                        oldColumnHeader, columnHeader);
                resizeColumns();

            }
        }
        
    }
    
    private void resizeColumns() {
        resizeColumnsAction.actionPerformed(new ActionEvent(this, 
                ActionEvent.ACTION_PERFORMED, null));
    }
    
    public void addUndoableEditListener(UndoableEditListener l) {
         undoSupport.addUndoableEditListener(l);
    }

    public void removeUndoableEditListener(UndoableEditListener l) {
         undoSupport.removeUndoableEditListener(l);
    }
    
    protected void fireUndoableColumnHeaderUpdate(int dataModelColumn, 
            String oldColumnHeader, String newColumnHeader) {
        
         UndoableEdit edit = new EditColumnHeaderAction
                 .UndoableColumnHeaderUpdate(
                    dataModelColumn, oldColumnHeader, newColumnHeader);
         undoSupport.beginUpdate();
         undoSupport.postEdit( edit );
         undoSupport.endUpdate();
         
     }
    
    /* Inner Classes */
     public class UndoableColumnHeaderUpdate extends AbstractUndoableEdit {
        
        /* Properties */
        private int oldDataModelColumn;
        private String oldColumnHeader;
        private String newColumnHeader;
        
        
        /* Methods */
        public UndoableColumnHeaderUpdate(int oldDataModelColumn, 
                String oldColumnHeader, String newColumnHeader) {
            
            this.oldDataModelColumn = oldDataModelColumn;
            this.oldColumnHeader = oldColumnHeader;
            this.newColumnHeader = newColumnHeader;

        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            dataTableModel.setColumnName(oldColumnHeader, oldDataModelColumn);
            resizeColumns();
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            dataTableModel.setColumnName(newColumnHeader, oldDataModelColumn);
            resizeColumns();
        }
        
    }
    
}
